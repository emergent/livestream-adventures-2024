// a general-purpose setup file
(
s.options.memSize_(2.pow(16));
s.options.sampleRate_(44100);
s.options.inDevice_(
	//nil;
	"Windows WASAPI : Line (USB AUDIO  CODEC)"
);
s.options.outDevice_(
	//nil;
	"Windows WASAPI : Lautsprecher (Conexant SmartAudio HD)"
);

s.newBusAllocators;
ServerBoot.removeAll;
ServerTree.removeAll;
ServerQuit.removeAll;

~out = 0;
t = TempoClock(60/60).permanent_(true);

~makeBuses = {
	~bus = Dictionary.new;
	~bus.add(\reverb -> Bus.audio(s, 2));
};

~makeNodes = {
	s.bind({
		~src = Group.new;
		~efx = Group.after(~src);

		~revSynth = Synth(\reverb, [
			\in, ~bus[\reverb],
			\revAmp, -3.dbamp,
			\out, ~out
		], ~efx);
	});
};

~cleanup = {
	"see you!".postln;
	Window.closeAll;
	Buffer.freeAll;
	s.newBusAllocators;
	ServerBoot.removeAll;
	ServerTree.removeAll;
	ServerQuit.removeAll;
};

//////////// register functions
ServerBoot.add(~makeBuses);
ServerQuit.add(~cleanup);

s.waitForBoot({
	//s.plotTree;
	//s.meter.window.alwaysOnTop_(true);

	s.sync;

	SynthDef(\reverb, {
			var sig;
			sig = In.ar(\in.ir(0), 2);
			sig = JPverb.ar(
					sig,
					\rtime.kr(4),
			\damp.kr(0.75),
			\size.kr(4.5),
			\earlyDiff.kr(0.8),
			\modDepth.kr(0.12),
			\modFreq.kr(2),
			\low.kr(1),
			\mid.kr(0.9),
			\high.kr(0.8)
		);
		sig = sig * \revAmp.kr(0.5);
		Out.ar(\out.ir(~out), sig);
	}).add;

	s.sync;
	ServerTree.add(~makeNodes);

	s.freeAll;
	s.sync;

	"do eet".postln;
});
)


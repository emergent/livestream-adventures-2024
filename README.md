# Livestream Adventures 2024
This is the code I write during my somewhat irregular livestreams. 
Use it at your own discretion.

## Dependencies
 * sc3-plugins
 * MiUgens
 * PortedPlugins
